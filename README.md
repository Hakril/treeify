#treeify : transform any directory listing into a "tree output"

This small script read a 'directory listing' on the standard into and output it in a "tree format".

## Example

    :::shell
    echo "file1\n
         dir1/file2\n
         dir1/file3\n
         dir1/dir2/file4" | python ./treeify.py

    .
    ├─ file1
    └─ dir1
      ├─ file3
      ├─ file2
      └─ dir2
        └─ file4

## Use case:

### git
treeify can be useful with the command "git ls-files" that show informations about files followed by git in a directory.
treeify allow to get this list with a nice display.

Here is the git-alias I use:

    [alias]
        tree = !git ls-files $GIT_PREFIX | sed -e "s%^$GIT_PREFIX%%" | python ~/treeify/treeify.py

Demo:

    :::shell
    hakril@demo % ls
    git_file1  git_file2  other_file
    hakril@demo % git tree
    .
    ├─ git_file2
    └─ git_file1

### Improvements

If you have any other use cases or any ideas for improvements : let me know.

### Contact

 - hakril@lse.epita.fr
 - Twitter: @hakril
